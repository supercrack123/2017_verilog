`timescale 1ps/1ps

module MUX2TO1(A,B,C,Z);
  input A,B,C; // carry 0 4 bit adder : A, carry 1 4 bit adder : B, C = Cin 
  output Z; // each SUM bit
  wire w1,w2,notc;
  mnot nc(notc,C);
  mand2 mux0(w1,B,C);
  mand2 mux1(w2,A,notc);
  mor2 mux2(Z,w1,w2); // Z = BC + AC`

endmodule

