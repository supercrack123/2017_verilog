`timescale 1ns/1ns
module testbench;
    
  reg [7:0] a8;
  reg [7:0] b8;
  wire [7:0] o_csa8;
  wire [7:0] o_cla8;

  CSA8 mcsa8(o_csa8, a8, b8);
  CLA8 mcla8(o_cla8, a8, b8);
    
  initial begin  // substitute always as initial because last testcase is not showed. and #1 -> #2 because most delay time is over than 1ns. 
    #0  a8=8'b11111111; b8=8'b00000001; // most delay time
    #2  a8=8'b00011111; b8=8'b00001111;
    #2  a8=8'b00001111; b8=8'b00000111;
    #2  a8=8'b00000000; b8=8'b00000000;
    #2  a8=8'b00000001; b8=8'b00000010; // least delay time
    #2  a8=8'b00000001; b8=8'b00000001;
    #2  a8=8'b00010001; b8=8'b00010001;
    #2  a8=8'b01010101; b8=8'b01010101;
  end
  
endmodule