`timescale 1ps/1ps
module Bit_Adder2_CLA(a,b,cin,p,g,sum); //2 fan-in bit adder for Carry_lookahead adder

input [1:0] a,b; 
input cin;
output [1:0] p ,g; // carry propagate function, carry generate function variable
output [1:0] sum;
wire carry;
wire cp; //Ci+1 = Ci + CiPi , CiPi

mxor2 p0(p[0],a[0],b[0]);
mxor2 p1(p[1],a[1],b[1]);
mand2 g0(g[0],a[0],b[0]);
mand2 g1(g[1],a[1],b[1]);

mand2 CP0(cp,cin,p[0]);  //CiPi
mor2 C0(carry,g[0],cp); //Ci+1 = Ci + CiPi


mxor2 SUM0(sum[0],p[0],cin);
mxor2 SUM1(sum[1],p[1],carry);

endmodule
