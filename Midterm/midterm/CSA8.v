`timescale 1ps/1ps

module CSA8(sum,a,b);
input [7:0]a,b; //8-bit inputs
output [7:0]sum; //8-bit outputs
wire [2:0] tempcout; // for 8 bit sum , each Bit_Adder4_CSA carry out 
wire [3:0] tempsum1, tempsum2; //  carry 0 Bit_Adder4_CSA SUM and carry 1 Bit_Adder4_CSA SUM
// wire circout, cout; used by for last cout bit

Bit_Adder4_CSA level1(a[3:0],b[3:0],0,sum[3:0],tempcout[0]);
Bit_Adder4_CSA level2(a[7:4],b[7:4],0,tempsum1[3:0],tempcout[1]);
Bit_Adder4_CSA level3(a[7:4],b[7:4],1,tempsum2[3:0],tempcout[2]);

MUX2TO1 M1(tempsum1[0],tempsum2[0],tempcout[0],sum[4]);
MUX2TO1 M2(tempsum1[1],tempsum2[1],tempcout[0],sum[5]);
MUX2TO1 M3(tempsum1[2],tempsum2[2],tempcout[0],sum[6]);
MUX2TO1 M4(tempsum1[3],tempsum2[3],tempcout[0],sum[7]);

//mor2 cout1(circout,tempcout[0],tempcout[1]);
//mand2 cout2(cout,circout,tempcout[2]); last Cout bit arithmetic

endmodule


/*
`timescale 1ps/1ps
module CSA8(sum,a,b);
input [7:0] a,b;
output [7:0] sum;
wire [3:0] cout;
wire [3:0] p,g,temp1,temp2,temg1,temg2; //carry parapagate, carry generate function variable
wire [3:0] tempsum1, tempsum2; //  carry 0 Bit_Adder4_CSA SUM and carry 1 Bit_Adder4_CSA SUM
// wire circout, cout; used by for last cout bit

Bit_Adder4_CLA bitadd1(a[3:0],b[3:0],0,p[3:0],g[3:0],sum[3:0]);  // A3,A2,A1,A0 + B3,B2,B1,B0
carryunit carry1(0,p[0],g[0],cout[0]);
carryunit carry2(cout[0],p[1],g[1],cout[1]);
carryunit carry3(cout[1],p[2],g[2],cout[2]);
carryunit carry4(cout[2],p[3],g[3],cout[3]);
Bit_Adder4_CLA bitadd2(a[7:4],b[7:4],0,temp1[3:0],temg1[3:0],tempsum1[3:0]);
Bit_Adder4_CLA bitadd3(a[7:4],b[7:4],1,temp2[3:0],temg2[3:0],tempsum2[3:0]);

MUX2TO1 M1(tempsum1[0],tempsum2[0],cout[3],sum[4]);
MUX2TO1 M2(tempsum1[1],tempsum2[1],cout[3],sum[5]);
MUX2TO1 M3(tempsum1[2],tempsum2[2],cout[3],sum[6]);
MUX2TO1 M4(tempsum1[3],tempsum2[3],cout[3],sum[7]);

endmodule
*/