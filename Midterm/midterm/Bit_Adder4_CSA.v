`timescale 1ps/1ps
module Bit_Adder4_CSA(A,B,Cin,Sum,Cout); //4bit Adder for carry select adder
input [3:0] A,B;
input Cin;
output [3:0] Sum;
output Cout;
wire [2:0] Cf; // each Full Adder's Carry bit

Full_Adder F1(A[0],B[0],Cin,Cf[0],Sum[0]);
Full_Adder F2(A[1],B[1],Cf[0],Cf[1],Sum[1]);
Full_Adder F3(A[2],B[2],Cf[1],Cf[2],Sum[2]);
Full_Adder F4(A[3],B[3],Cf[2],Cout,Sum[3]);

endmodule
