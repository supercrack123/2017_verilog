`timescale 1ps/1ps
module CLA8(sum,a,b);
input [7:0] a,b;
output [7:0] sum;
wire [3:0] cout;
wire [7:0] p,g; //carry parapagate, carry generate function variable

Bit_Adder4_CLA bitadd1(a[3:0],b[3:0],0,p[3:0],g[3:0],sum[3:0]);  // A3,A2,A1,A0 + B3,B2,B1,B0
carryunit carry1(0,p[0],g[0],cout[0]);
carryunit carry2(cout[0],p[1],g[1],cout[1]);
carryunit carry3(cout[1],p[2],g[2],cout[2]);
carryunit carry4(cout[2],p[3],g[3],cout[3]);
Bit_Adder4_CLA bitadd2(a[7:4],b[7:4],cout[3],p[7:4],g[7:4],sum[7:4]); //  A3,A2,A1,A0 + B3,B2,B1,B0 + CARRY

endmodule