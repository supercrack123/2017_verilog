`timescale 1ps/1ps

module Full_Adder(A,B,CIN,COUT,SUM); //Carry Select Adder's Full Adder for 4bit Adder
input A,B,CIN;
output COUT,SUM;
wire w1,w2,w3;
mxor3 sum(SUM,A,B,CIN); // Sum = A xor B xor Cin
mor2 cout1(w1,A,B);
mand2 cout2(w2,CIN,w1);
mand2 cout3(w3,A,B);
mor2 cout4(COUT,w2,w3); // Cout = AB + Cin(A+B)
endmodule

