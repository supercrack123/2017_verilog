`timescale 1ps/1ps
module Bit_Adder4_CLA(a,b,cin,p,g,sum);

input [3:0] a,b;
input cin;
output [3:0] sum;
output [3:0] p,g; //carry parapagate, carry generate function variable
wire [1:0] cout;
Bit_Adder2_CLA add1(a[1:0],b[1:0],cin,p[1:0],g[1:0],sum[1:0]); //A1,A0 + B1,B0
carryunit carry1(cin,p[0],g[0],cout[0]);
carryunit carry2(cout[0],p[1],g[1],cout[1]);
Bit_Adder2_CLA add2(a[3:2],b[3:2],cout[1],p[3:2],g[3:2],sum[3:2]); //A3,A2 + B3,B2 + CARRY

endmodule