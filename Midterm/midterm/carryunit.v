`timescale 1ps/1ps
module carryunit(cin,p,g,cout);
input cin, p, g;
output cout ;
wire cp;
mand2 CP(cp,cin,p);
mor2 COUT(cout,g,cp); //Ci+1 = Gi + CiPi
endmodule