module STAGECHANGER(clk,keydata,stage,switch,finish);
input clk;
input [5:0]keydata;
input switch;
input finish;
output [5:0]stage;

wire chan;
assign chan = keydata[0] | keydata[1] | keydata[2] | keydata[3] | keydata[4] | keydata[5]; 

reg[5:0] stage;
reg chan_reg;

initial stage = 1;


always @(posedge clk) 
begin 
	if(switch == 1) begin //go stopwatch mode
		case(stage) //initialize state to go stopwatch initial mode
		8: stage = 1;
		9: stage = 1;
		10: stage =1;
		11: stage =1;
		12: stage =1;
		default stage = stage;
		endcase
	end
	else begin // go timer mode
		case(stage) //initialize state to go timer initial mode
		1: stage =8;
		2: stage =8;
		3: stage =8;
		default stage = stage;
		endcase
	end
	chan_reg<=chan;
	if(chan>chan_reg)
	begin
		case(keydata) // AS #,* button, go another state
		11: begin // * button
			case(stage)
			3: stage =1;
			8: stage =9;
			9: stage = 10;
			12: stage = 8;
			
			default: stage = stage;
			endcase
			end
		12: begin // # button
			case(stage)
			1: stage = 2;
			2: stage = 3;
			3: stage = 2;
			10: stage = 11;
			11: stage = 12;
			12: stage = 11;
			13: stage = 8;
			default: stage = stage;
			endcase
			end
		default: stage = stage;
		endcase
	end
	if (finish==1) // go illuminate LED
		stage =13;

end

endmodule
