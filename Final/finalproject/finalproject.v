module finalproject(clk, key_row, key_col, seg_stage, seg_data, switch, LED);

input clk;
input [3:0]key_row;
input switch;

output [2:0]key_col;
output [0:5]seg_stage;
output [7:0]seg_data;
output [7:0]LED;

reg [0:5] seg_stage;
reg [7:0] seg_data;
reg chan_reg;
reg [7:0]time6;
reg [3:0] fnd_num;
reg [31:0] ledclk;



reg fnd_clk;
wire [5:0] key_data;
wire [5:0] stage;
reg [3:0] temp_time[5:0];


reg [15:0]cycle_cnt;
reg [7:0] LED;
reg [31:0] fnd_cnt;
reg [2:0] fnd_state;
reg [6:0] min;
reg [6:0]	sec;
reg [7:0]	tm_sec;
reg [3:0]	min_10, min_1, sec_10, sec_1, tm_sec_10, tm_sec_1;
 
reg finish;
wire chan;

assign chan = key_data[0] | key_data[1] | key_data[2] | key_data[3] | key_data[4] | key_data[5]; 



initial begin 

	finish = 0;
	cycle_cnt=0;
	fnd_clk=0;
	fnd_cnt =0;
	time6=0;
end

always @ (posedge clk) //Frequency divider
begin
	if (cycle_cnt == 0)			fnd_clk = 1;
	else if (cycle_cnt == 32)	fnd_clk = 0;
		
	cycle_cnt = cycle_cnt + 1;
	if (cycle_cnt == 64)		cycle_cnt = 0;

 end
	
KEYGET keyget(.clk(fnd_clk), .key_col(key_col), .key_row(key_row), .keydata(key_data));
STAGECHANGER changer(.clk(fnd_clk),.keydata(key_data),.stage(stage),.switch(switch),.finish(finish));





always @(posedge fnd_clk) // time clock
begin
	if(stage ==1 || stage ==8) // stopwatch, timer state initialization
	begin
		tm_sec=0;
		sec=0;
		min=0;
		tm_sec_1=0;
		tm_sec_10=0;
		sec_10=0;
		sec_1=0;
		min_1=0;
		min_10=0;
	end
	if(stage ==2 || stage == 11) // for increasing time mode, decreasing time mode
		fnd_cnt = fnd_cnt + 1;
	
	if(stage != 11 || stage !=13)
		finish = 0;

	if(stage==2) begin
	if (fnd_cnt == 5120)	begin	tm_sec = tm_sec + 1;	fnd_cnt = 0;	tm_sec_10 = tm_sec / 10;	tm_sec_1 = tm_sec % 10;	end
	if (tm_sec == 100)	begin	tm_sec = 0;				tm_sec_10 = 0;	tm_sec_1 = 0;				sec = sec + 1;			sec_10 = sec / 10;	sec_1 = sec % 10;	end
	if (sec == 60)			begin	sec = 0;				sec_10 = 0;		sec_1 = 0;					min = min + 1;			min_10 = min / 10;	min_1 = min % 10;	end
	if (min == 60)			begin 	min = 0;				min_10 = 0;		min_1 = 0;					end
   end
	else if(stage==11) begin 
	   if (fnd_cnt == 5120)	begin	tm_sec = tm_sec - 1;	fnd_cnt = 0;	tm_sec_10 = tm_sec / 10;	tm_sec_1 = tm_sec % 10;	end
		if (tm_sec == 8'b11111111)		begin	tm_sec = 99;				tm_sec_10 = 9;	tm_sec_1 = 9;				sec = sec - 1;			sec_10 = sec / 10;	sec_1 = sec % 10;	end
		if (sec == 7'b1111111)			begin	min = min - 1; if(min==7'b1111111) begin min=0; tm_sec_10=0; tm_sec_1=0; min_1=0; min_10=0; sec_10=0; sec_1=0; finish=1; end else begin sec = 59; sec_10 = 5; sec_1 = 9;	min_10 = min / 10;	min_1 = min % 10; end end
		if (min < 0)			begin 	min = 0;				min_10 = 0;		min_1 = 0; finish = 1; end
   end
	else if(stage==10) begin
		tm_sec=temp_time[1]*10 + temp_time[0];
		sec = temp_time[3]*10 + temp_time[2];
		min = temp_time[5]*10 + temp_time[4];
		tm_sec_10 = tm_sec / 10;	tm_sec_1 = tm_sec % 10;
		sec_10 = sec / 10;	sec_1 = sec % 10;
		min_10 = min / 10;	min_1 = min % 10;
	end
	// for 7segment data afterimage effect
	
		if 	  (fnd_state == 0)	begin	seg_stage = 6'b011111; if(stage==9) fnd_num = temp_time[5]; else	fnd_num = min_10;		fnd_state = 1;	end
		else if (fnd_state == 1)	begin	seg_stage = 6'b101111;	if(stage==9) fnd_num = temp_time[4]; else fnd_num = min_1;		fnd_state = 2;	end
		else if (fnd_state == 2)	begin	seg_stage = 6'b110111;	if(stage==9) fnd_num = temp_time[3]; else fnd_num = sec_10;		fnd_state = 3;	end
		else if (fnd_state == 3)	begin	seg_stage = 6'b111011;	if(stage==9) fnd_num = temp_time[2]; else fnd_num = sec_1;		fnd_state = 4;	end
		else if (fnd_state == 4)	begin	seg_stage = 6'b111101;	if(stage==9) fnd_num = temp_time[1]; else fnd_num = tm_sec_10;	fnd_state = 5;	end
		else if (fnd_state == 5)	begin	seg_stage = 6'b111110;	if(stage==9) fnd_num = temp_time[0]; else fnd_num = tm_sec_1;		fnd_state = 0;	end
		

		case (fnd_num) // for 7segment data
			0 : begin seg_data = 8'b11111100; end
			1 : begin seg_data = 8'b01100000; end
			2 : begin seg_data = 8'b11011010; end
			3 : begin seg_data = 8'b11110010; end
			4 : begin seg_data = 8'b01100110; end
			5 : begin seg_data = 8'b10110110; end
			6 : begin seg_data = 8'b00111110; end
			7 : begin seg_data = 8'b11100000; end
			8 : begin seg_data = 8'b11111110; end
			9 : begin seg_data = 8'b11100110; end
		endcase
	

end


always @(posedge clk) 
begin 
	chan_reg<=chan;
	if(chan>chan_reg)
	begin
	if(key_data==11||key_data==12);
	else // get user's time number
	begin
	if(stage==9) begin
	  case(time6)
	    0: begin temp_time[5] = 0; temp_time[4] = 0; temp_time[3] = 0; temp_time[2] = 0; temp_time[1] = 0; temp_time[0] = key_data%10; time6=1; end
	    1: begin temp_time[5] = 0; temp_time[4] = 0; temp_time[3] = 0; temp_time[2] = 0; temp_time[1] = temp_time[0]; temp_time[0] =key_data%10; time6=2; end
	    2: begin temp_time[5] = 0; temp_time[4] = 0; temp_time[3] = 0; temp_time[2] = temp_time[1]; temp_time[1] = temp_time[0]; temp_time[0] =key_data%10; time6=3; end
	    3: begin temp_time[5] = 0; temp_time[4] = 0; temp_time[3] = temp_time[2]; temp_time[2] = temp_time[1]; temp_time[1] = temp_time[0]; temp_time[0] = key_data%10; time6=4; end
	    4: begin temp_time[5] = 0; temp_time[4] = temp_time[3]; temp_time[3] = temp_time[2]; temp_time[2] = temp_time[1]; temp_time[1] = temp_time[0]; temp_time[0] = key_data%10; time6=5; end
	    5: begin temp_time[5] = temp_time[4]; temp_time[4] = temp_time[3]; temp_time[3] = temp_time[2]; temp_time[2] = temp_time[1]; temp_time[1] = temp_time[0]; temp_time[0] = key_data%10; time6=0; end
	    endcase
	  end
	 end
	 end
	 if(stage ==8) begin
		temp_time[0]=0;
		temp_time[1]=0;
		temp_time[2]=0;
		temp_time[3]=0;
		temp_time[4]=0;
		temp_time[5]=0;
	 end
		
	

end

always @(posedge fnd_clk) // LED blinks every second
begin
	if(stage==13)
		ledclk=ledclk+1;
	else
		ledclk = 0;
	
	if(ledclk <256000)
		LED=0;
	else if(ledclk<512000)
		LED=8'b11111111;
	else ledclk = 0;
	
end

endmodule
