module KEYGET(clk, key_col, key_row, keydata);
input clk;  //clk = 10kHz 
input  [3:0]  key_row; 
output [2:0]  key_col; 
output [5:0] keydata; 
reg    [5:0] keydata; 
reg    [2:0]  state; 
wire          chan;
assign chan=key_row[0] | key_row[1] | key_row[2] | key_row[3]; //recognize button input
assign key_col = state;

//state change variables
parameter no_scan = 3'b000; 
parameter column1 = 3'b001; 
parameter column2 = 3'b010; 
parameter column3 = 3'b100;

//as change the clock, change key_state  
always @(posedge clk) 
begin 
if (!chan) 
	begin 
	case (state) 
		no_scan  : state <= column1; 
		column1  : state <= column2; 
		column2  : state <= column3; 
		column3  : state <= column1; 
		default: state <= no_scan; 
	endcase 
	end
end

//if row and column value fit input, take the adequate value
always @(posedge clk) 
begin 
	case (state) 
		column1 : 
			case (key_row)
			4'b0001 : keydata <= 1; //key_1 
			4'b0010 : keydata <= 4; //key_4 
			4'b0100 : keydata <= 7; //key_7 
			4'b1000 : keydata <= 11; //key_* 
			default: keydata <= 0; 
			endcase
		column2 : 
			case (key_row)
			4'b0001 : keydata <= 2; //key_2 
			4'b0010 : keydata <= 5; //key_5 
			4'b0100 : keydata <= 8; //key_8 
			4'b1000 : keydata <= 10; //key_0
			default: keydata <= 0; 
			endcase
		column3 : 
			case (key_row)
			4'b0001 : keydata <= 3; //key_3 
			4'b0010 : keydata <= 6; //key_6 
			4'b0100 : keydata <= 9; //key_9 
			4'b1000 : keydata <= 12; //key_* 
			default: keydata <= 0; 
			endcase
		default: keydata<=0;
	endcase 
end
			
			
		


endmodule
